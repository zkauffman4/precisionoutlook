﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.IO;
using Newtonsoft.Json;

namespace Precision
{
    public partial class SettingsForm : Form
    {
        public string errorMessage;
        public static CookieContainer cookies;
        public SettingsForm()
        {
            InitializeComponent();
            if (errorMessage != null)
            {
                MessageBox.Show(errorMessage);
            }
            if (Properties.Settings.Default.PrecisionUsername.Length > 0)
            {
                emailTextbox.Text = Properties.Settings.Default.PrecisionUsername;
            }
            if (Properties.Settings.Default.PrecisionPassword.Length > 0)
            {
                passwordTextbox.Text = Properties.Settings.Default.PrecisionPassword;
            }
        }

        public static bool loginWithDefaults()
        {
            SettingsForm.cookies = new CookieContainer();
            string URLAuth = "https://precisiontimeentry.com/api/login.php";
            string username = Properties.Settings.Default.PrecisionUsername;
            string pass = Properties.Settings.Default.PrecisionPassword;
            string postString = string.Format("u={0}&p={1}", username, pass);

            const string contentType = "application/x-www-form-urlencoded";
            System.Net.ServicePointManager.Expect100Continue = false;

            HttpWebRequest webRequest = WebRequest.Create(URLAuth) as HttpWebRequest;
            webRequest.Method = "POST";
            webRequest.ContentType = contentType;
            webRequest.CookieContainer = SettingsForm.cookies;
            webRequest.ContentLength = postString.Length;
            webRequest.UserAgent = "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1";
            webRequest.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";

            StreamWriter requestWriter = new StreamWriter(webRequest.GetRequestStream());
            requestWriter.Write(postString);
            requestWriter.Close();
            StreamReader responseReader = new StreamReader(webRequest.GetResponse().GetResponseStream());
            string responseData = responseReader.ReadToEnd();

            responseReader.Close();
            webRequest.GetResponse().Close();

            try
            {
                UserModel u = (UserModel)JsonConvert.DeserializeObject(responseData, typeof(UserModel));
                if (u.Username != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                return false;
            }

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Properties.Settings.Default.PrecisionUsername = emailTextbox.Text;
            Properties.Settings.Default.PrecisionPassword = passwordTextbox.Text;

            bool loginSuccess = SettingsForm.loginWithDefaults();
            if (!loginSuccess)
            {
                MessageBox.Show("There was an error logging in, please try again!", "Login Error");
            }
            else
            {
                EntryForm ef = new EntryForm();
                ef.cookieContainer = SettingsForm.cookies;
                ef.Show();
                this.Close();
            }
            
            
        }
    }
}
