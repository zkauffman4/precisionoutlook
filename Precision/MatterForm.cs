﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Precision
{
    public partial class MatterForm : Form
    {
        private List<MatterModel> matters, filteredList;
        bool isFiltered = false;
        public EntryForm parent;
        public MatterForm(EntryForm ef)
        {
            InitializeComponent();
            parent = ef;
            matterListView.RowHeadersVisible = false;
            const string contentType = "application/x-www-form-urlencoded";
            System.Net.ServicePointManager.Expect100Continue = false;
            string matterUrl = "https://precisiontimeentry.com/api/get_matters.php";
            HttpWebRequest webRequest = WebRequest.Create(matterUrl) as HttpWebRequest;
            webRequest.Method = "POST";
            webRequest.ContentLength = 0;
            webRequest.ContentType = contentType;
            webRequest.CookieContainer = SettingsForm.cookies;
            webRequest.UserAgent = "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1";
            webRequest.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";


            StreamReader responseReader = new StreamReader(webRequest.GetResponse().GetResponseStream());
            string responseData = responseReader.ReadToEnd();

            responseReader.Close();
            webRequest.GetResponse().Close();
            try
            {
                //List<MatterModel> matters = JsonConvert.DeserializeObject<List<MatterModel>>(responseData);
                MatterList l = (MatterList)JsonConvert.DeserializeObject(responseData, typeof(MatterList));
                this.matters = l.Matters.ToList();
                matterListView.DataSource = matters;
                /*foreach(MatterModel m in this.matters)
                {
                    ListViewItem lvi = new ListViewItem(m.MatterName);
                    lvi.SubItems.Add(m.Id);
                    matterListView.Items.Add(lvi);
                }*/
                
            }
            catch (Exception e)
            {

            }
        }

        private void searchBarFilter(object sender, EventArgs e)
        {
            if (((TextBox)sender).Text != null && ((TextBox)sender).Text.Length > 0)
            {
                isFiltered = true;
                filteredList = new List<MatterModel>();
                foreach (MatterModel m in matters)
                {
                    if (m.MatterName.ToLowerInvariant().Contains(((TextBox)sender).Text.ToLowerInvariant()) || m.Id.ToLowerInvariant().Contains(((TextBox)sender).Text.ToLowerInvariant()))
                    {
                        filteredList.Add(m);
                    }
                }
                matterListView.DataSource = filteredList;
            }
            else
            {
                isFiltered = false;
                matterListView.DataSource = matters;
            }
        }

        private void matterListView_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            MatterModel selected;
           if(isFiltered)
           {
               selected = filteredList[e.RowIndex];
           }
           else
           {
               selected = matters[e.RowIndex];
           }
           this.parent.setSelectedMatter(selected);
           this.Close();
            //send back to parent
        }
    }
}
