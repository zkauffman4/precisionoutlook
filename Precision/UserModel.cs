﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Precision
{
    class UserModel
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("matter_id")]
        public string MatterId { get; set; }

        [JsonProperty("username")]
        public string Username { get; set; }

        [JsonProperty("role")]
        public string Role { get; set; }
    }
}
