﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using Newtonsoft.Json;
using System.IO;
using System.Text.RegularExpressions;

namespace Precision
{
    public partial class EntryForm : Form
    {
        AutoCompleteStringCollection namesCollection = new AutoCompleteStringCollection();
        public CookieContainer cookieContainer;
        private bool _canUpdate = true;
        List<MatterModel> matters;
        private string textToDisplay;
        private EventHandler matterHandler, taskHandler, componentHandler;
        public MatterModel selectedMatter;

        private bool _needUpdate = false;
        public EntryForm()
        {
            InitializeComponent();

            //fill all dropdowns here
            
            this.cookieContainer = SettingsForm.cookies;
            populateCombos();
            //this.matterCombo.TextChanged += matterCombo_TextChanged;
            //this.matterCombo.SelectedIndexChanged += matterCombo_SelectedIndexChanged;
            //this.matterCombo.TextUpdate +=matterCombo_TextUpdate;


        }


        public void populateCombos()
        {
            var dataSource = new List<Hour>();
            dataSource.Add(new Hour() { Name = "0 hours", Value = "0" });
            dataSource.Add(new Hour() { Name = "1 hour", Value = "1" });
            dataSource.Add(new Hour() { Name = "2 hours", Value = "2" });
            dataSource.Add(new Hour() { Name = "3 hours", Value = "3" });
            dataSource.Add(new Hour() { Name = "4 hours", Value = "4" });
            dataSource.Add(new Hour() { Name = "5 hours", Value = "5" });
            dataSource.Add(new Hour() { Name = "6 hours", Value = "6" });
            dataSource.Add(new Hour() { Name = "7 hours", Value = "7" });
            dataSource.Add(new Hour() { Name = "8 hours", Value = "8" });

            this.hoursCombo.DataSource = dataSource;
            this.hoursCombo.DisplayMember = "Name";
            this.hoursCombo.ValueMember = "Value";
            this.hoursCombo.DropDownStyle = ComboBoxStyle.DropDownList;

            var minutesDataSource = new List<Minute>();
            minutesDataSource.Add(new Minute() { Name = "0 minutes", Value = "0" });
            minutesDataSource.Add(new Minute() { Name = "6 minutes", Value = "0" });
            minutesDataSource.Add(new Minute() { Name = "12 minutes", Value = "0" });
            minutesDataSource.Add(new Minute() { Name = "15 minutes", Value = "0" });
            minutesDataSource.Add(new Minute() { Name = "18 minutes", Value = "0" });
            minutesDataSource.Add(new Minute() { Name = "24 minutes", Value = "0" });
            minutesDataSource.Add(new Minute() { Name = "30 minutes", Value = "0" });
            minutesDataSource.Add(new Minute() { Name = "36 minutes", Value = "0" });
            minutesDataSource.Add(new Minute() { Name = "42 minutes", Value = "0" });
            minutesDataSource.Add(new Minute() { Name = "45 minutes", Value = "0" });
            minutesDataSource.Add(new Minute() { Name = "48 minutes", Value = "0" });
            minutesDataSource.Add(new Minute() { Name = "54 minutes", Value = "0" });

            this.minutesCombo.DataSource = minutesDataSource;
            this.minutesCombo.DisplayMember = "Name";
            this.minutesCombo.ValueMember = "Value";
            this.minutesCombo.DropDownStyle = ComboBoxStyle.DropDownList;

            

        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            SettingsForm sf = new SettingsForm();
            sf.Show();
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            bool reauth = SettingsForm.loginWithDefaults();
            if (reauth)
            {
                this.cookieContainer = SettingsForm.cookies;
                string URLAuth = "https://precisiontimeentry.com/api/new_entry.php";
                string matterId, hours, minutes, task, component, date, description;
                try
                {
                    //matterId = matterCombo.SelectedValue.ToString();
                }
                catch (Exception ex)
                {
                    matterId = "";
                }
                try
                {
                    hours = hoursCombo.SelectedValue.ToString();
                }
                catch (Exception ex)
                {
                    hours = "";
                }
                try
                {
                    minutes = minutesCombo.SelectedValue.ToString();
                }
                catch (Exception ex)
                {
                    minutes = "";
                }
                try
                {
                    task = taskCombo.SelectedValue.ToString();
                }
                catch (Exception ex)
                {
                    task = "";
                }
                try
                {
                    component = componentCombo.SelectedValue.ToString();
                }
                catch (Exception ex)
                {
                    component = "";
                }
                try
                {
                    date = dateWorked.Value.ToShortDateString();
                }
                catch (Exception ex)
                {
                    date = "";
                }
                try
                {
                    description = descriptionText.Text;
                }
                catch (Exception ex)
                {
                    description = "";
                }

                if ((hours.Equals("0") && minutes.Equals("0")) || task.Length < 1 || component.Length < 1 || date.Length < 1
                    || description.Length < 1)
                {
                    MessageBox.Show("Please fill out all fields!", "Submission Error");
                }
                else
                {

                    string postString = string.Format("matter={0}&hours={1}&minutes={2}&lcode={3}&acode={4}&date={5}&description={6}", "", hours, minutes, task, component, date, description);

                    const string contentType = "application/x-www-form-urlencoded";
                    System.Net.ServicePointManager.Expect100Continue = false;

                    HttpWebRequest webRequest = WebRequest.Create(URLAuth) as HttpWebRequest;
                    webRequest.Method = "POST";
                    webRequest.ContentType = contentType;
                    webRequest.CookieContainer = cookieContainer;
                    webRequest.ContentLength = postString.Length;
                    webRequest.UserAgent = "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1";
                    webRequest.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";

                    StreamWriter requestWriter = new StreamWriter(webRequest.GetRequestStream());
                    requestWriter.Write(postString);
                    requestWriter.Close();
                    StreamReader responseReader = new StreamReader(webRequest.GetResponse().GetResponseStream());
                    string responseData = responseReader.ReadToEnd();
                    responseReader.Close();
                    webRequest.GetResponse().Close();
                    if (responseData.Contains("success"))
                    {
                        this.Close();
                    }
                    else
                    {
                        MessageBox.Show("There was an error trying to save your entry. Please try again.");
                    }

                }
            }
            else
            {
                SettingsForm sf = new SettingsForm();
                sf.Show();
                this.Close();
            }
        }

       

        private void matterBtn_Click(object sender, EventArgs e)
        {
            MatterForm mf = new MatterForm(this);
            mf.Show();
        }

       public void setSelectedMatter(MatterModel m)
        {
            this.selectedMatter = m;
            matterBtn.Text = m.MatterName;
           //update task and component later
        }

    }
}
