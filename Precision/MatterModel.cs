﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Precision
{
    public class MatterModel
    {
        [JsonProperty("matter_id")]
        public string Id { get; set; }

        [JsonProperty("matter_name")]
        public string MatterName{ get; set; }

    }
}

