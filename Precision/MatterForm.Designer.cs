﻿namespace Precision
{
    partial class MatterForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.matterSearchBar = new System.Windows.Forms.TextBox();
            this.matterListView = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.matterListView)).BeginInit();
            this.SuspendLayout();
            // 
            // matterSearchBar
            // 
            this.matterSearchBar.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.matterSearchBar.Location = new System.Drawing.Point(12, 23);
            this.matterSearchBar.Name = "matterSearchBar";
            this.matterSearchBar.Size = new System.Drawing.Size(415, 32);
            this.matterSearchBar.TabIndex = 0;
            this.matterSearchBar.TextChanged += new System.EventHandler(this.searchBarFilter);
            // 
            // matterListView
            // 
            this.matterListView.AllowUserToAddRows = false;
            this.matterListView.AllowUserToDeleteRows = false;
            this.matterListView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.matterListView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.matterListView.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.matterListView.Location = new System.Drawing.Point(12, 81);
            this.matterListView.MultiSelect = false;
            this.matterListView.Name = "matterListView";
            this.matterListView.ReadOnly = true;
            this.matterListView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.matterListView.Size = new System.Drawing.Size(414, 167);
            this.matterListView.TabIndex = 1;
            this.matterListView.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.matterListView_CellDoubleClick);
            // 
            // MatterForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Highlight;
            this.ClientSize = new System.Drawing.Size(439, 261);
            this.Controls.Add(this.matterListView);
            this.Controls.Add(this.matterSearchBar);
            this.Name = "MatterForm";
            this.Text = "MatterForm";
            ((System.ComponentModel.ISupportInitialize)(this.matterListView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox matterSearchBar;
        private System.Windows.Forms.DataGridView matterListView;
    }
}